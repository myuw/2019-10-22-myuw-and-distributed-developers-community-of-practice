# 2019-10-22-myuw-and-distributed-developers-community-of-practice

Logistics:

2019-10-22
1p to 2:30p
BioCommons 110 (Steenbck library)

Talk description:

Using MyUW to help users discover, understand, navigate, and use IT solutions
developed by members of the distributed developer community-of-practice.

+ Explained
+ Questions answered about
+ Demonstrated

Agenda:

+ Prepared content (40m)
+ Questions, discussion, distributed developer use cases and how MyUW might
  help (20m)
+ Build prototypes addressing distributed developer use cases (we'll hands-on,
  for real, build data and configuration in a non-prod tier to demonstrate how
  MyUW might help) (30m)

Stay for as much or as little of this as you find valuable.

Advance questions:

+ What are you building, what services are you delivering, that you wish the
  target audience were more aware of?
+ What IT solutions are hard to find again, having found them once?
+ What do your users need to know to inform their choices about when and how
  how much to engage more?

Advance hints about what you're interested in can inform the examples and how
deeply we dive into what...

[Slideware][] as published (work-in-progress).
[Slideware source][] (work-in-progress).

[Slideware]: https://docs.google.com/presentation/d/e/2PACX-1vTkDRkm9SrPSDwMEGzwG5wdzipXL4xOOk-SLFY2o1f0VBem7KoU6QS11jG24um2SNJHCmaWnIYqBYgb/pub?start=false&loop=false&delayms=60000
[Slideware source]: https://docs.google.com/presentation/d/1U0bqAZ5i3DoaCK8dMrOQj1je7XaI1a5x1So5YVwq4KM/edit
