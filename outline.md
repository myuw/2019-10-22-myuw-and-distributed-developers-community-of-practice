# Outline of 2019-10-22 MyUW and Distributed Developers Community of Practice talk

## Introduction

### MyUW: who

+ MyUW Infrastructure Team
++ Operate and develop the MyUW service
++ <myuw-infra@office365.wisc.edu>
++ DoIT -> AIS -> WPS -> MyUW

### Partners

+ MyUW Academic Applications
+ UW Flexible Option
++ Marcella Thompson, previously Joyce Johnston
+ Service Center
+ ...

### MyUW vision

**Ease and clarity in a complex digital environment**

We envision MyUW as leverage for thriving.

It’s a laudable vision. It’s never gonna be fully achieved.

So what do we do about
laudable visions that are never going to be fully achieved?

We work towards them, today, now, doing what we can.
That’s mission.

### MyUW mission

MyUW supports university participants in

+ discovering,
+ understanding,
+ navigating, and
+ performing university-related tasks.

MyUW is how I start getting a university thing done.

### Introductions

Who is here?

+ Name
+ How you serve the public
+ Something at the university,
  maybe related to your service,
  that's important to you

### MyUW vision: reprise

Ease and clarity in a complex digital environment

We envision the university supporting its participants in making the most of their university experience.

We envision MyUW as leverage for thriving.

### MyUW mission: reprise

MyUW supports university participants in

+ discovering,
+ understanding,
+ navigating, and
+ performing university-related tasks.

MyUW is how I start getting a university thing done.

### You

+ You: (probably) Deliver services supporting university-related tasks.

### You + MyUW

+ You: Deliver services supporting university-related tasks.
+ MyUW: help users discover, understand, navigate, use those services.

MyUW works best when it adds leverage to IT investments.

## Aside: why is this awkward?

So, it was healthy volunteering to come speak with you about MyUW.

And it's also awkward.

Because it made me realize, we're currently kind of optimized for two modes:

+ Full service
+ Close, active partnership

And while I firmly believe the hypothesis that MyUW could be adding a lot of
value to distributed dev work...

we're not currently good at this meeting in the middle.

Let's not let that stop us. Let's learn from it. And learn to do this better.

Nonetheless, without further ado, let's take a look at what MyUW can do for you
today.

## Discovery

MyUW supports university participants in

+ **discovering**,
+ understanding,
+ navigating, and
+ performing

university-related tasks.

### MyUW usage

Many people access MyUW often.
Frequency of use, that users will be accessing MyUW anyway, helps make MyUW
more effective at helping users discover more services.

### MyUW satisfaction

A somewhat dated survey of staff and student satisfaction with DoIT services
indicated users were pretty satsified with MyUW.

### Personalized home page

MyUW delivers a personalized home.

Which widgets you get by default depends on your identity. Group memberships.

So at its most basic, users might discover things because those things appeared
on their home page by default.

This flexes with identity changes.
As students progress from applicant to accepted and then an enrolled student,
and then maybe a student employee,
their home page changes to reflect what they gain and lose access to.

### App directory

The stuff on the home page, those are the widget views on entries in the app
directory.

Not everything can fit on the home page. There's many more entries in the
directory than any user gets on their home page by default.

### Search

Continuing that theme of how MyUW helps users discover relevant services,
you can of course search.

Searching, you might discover something useful.

### Search optimization

We can pepper MyUW app directory entries with `keywords`.
These will make the items appear in matching searches
regardless of title, description, other fields.
This is useful for common typos, other names for services.


### Browse

You can also browse through the directory of stuff.

What you see in the directory is limited to what MyUW thinks you have access to.

Browsing, you might discover something useful.

### Messages

Besides tiles, widgets, content you can find via search,
MyUW also has an entirely different kind of thing: "messages". Notifications.

Notifying users about a thing can help them discover that it's a thing.

## Understanding

MyUW helps users discover services. Cool, cool.
How can we help users understand the services?
What they're for, that they're relevant,
maybe what's up with them or how urgent it might be to exercise them.

### Descriptions

One of the most basic things we can do is describe services.

### Widgets

Sometimes more about a service is worth surfacing right on its widget. This can
help users understand what the service is and what it can do.





### Notifications

JSON callbacks

## Navigating

### App directory entries as hyperlinks

Compact mode as demonstration.

### Sometimes with more hyperlinks

### Or with search launchers

### Hyperlinks to app directory entries

App directory entries are hyperlinkable.

## Performing

### Do I have enough $$$ to meet a colleague for lunch?


### Log into O365 OWA as a service account




## User-centered

Solve real problems users care about.

Test with real users.

## Mechanics

### Get in touch

### Widget types

### Widget creator

Say you wanted to mock up

3 link option:

```json
{
  "launchText": "Open website",
  "links":[
    {
      "title":"Extension Intranet",
      "href":"https://uwprod.sharepoint.com/sites/Extension",
      "icon":"home",
      "target":"_blank"
    },
    {
      "title":"Extension Employee Handbook",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"menu_book",
      "target":"_blank"
    },
    {
      "title":"Extension Website",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"web",
      "target":"_blank"
    }
  ]
}
```

```json
{
  "launchText": "Open website",
  "links":[
    {
      "title":"Intranet",
      "href":"https://uwprod.sharepoint.com/sites/Extension",
      "icon":"home",
      "target":"_blank"
    },
    {
      "title":"Employee Handbook",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"menu_book",
      "target":"_blank"
    },
    {
      "title":"Website",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"web",
      "target":"_blank"
    }
  ]
}
```

Or maybe with 5:

```json
{
  "launchText": "Open website",
  "links":[
    {
      "title":"Intranet",
      "href":"https://uwprod.sharepoint.com/sites/Extension",
      "icon":"home",
      "target":"_blank"
    },
    {
      "title":"Employee Handbook",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"menu_book",
      "target":"_blank"
    },
    {
      "title":"Website",
      "href":"https://kb.wisc.edu/extension/",
      "icon":"web",
      "target":"_blank"
    },
    {
      "title":"Dean's blog",
      "href":"https://blogs.extension.wisc.edu/dean/",
      "icon":"school",
      "target":"_blank"
    },
    {
      "title":"Program Communications",
      "href":"https://blogs.extension.wisc.edu/programcomms/",
      "icon":"message",
      "target":"_blank"
    }
  ]
}
```





If we add 2 more it would be:

Extension Dean’s Blog: https://blogs.extension.wisc.edu/dean/

Extension Program Communications: https://blogs.extension.wisc.edu/programcomms/



### Custom widgets

### REST proxy and friends

### Entity files are XML with JSON in them

### MyUW tiers

### Messages are JSON

### Groups

### Web components

## Conclusions

You deliver IT solutions.
MyUW helps your users
Discover
Understand
Navigate
Do
Your IT solutions.
We want to help

Tremendous potential

But easy to get started

Get in touch
https://it.wisc.edu/services/myuw/

Let’s talk more
Come find us in Computer Sciences
myuw-infra@office365.wisc.edu
Channel in Teams
Questions, Comments, Concerns? (5 minutes)

Let’s talk more
improve university experiences
myuw-infra@office365.wisc.eduChannel in teams
Ease and clarity in a complex digital environment
