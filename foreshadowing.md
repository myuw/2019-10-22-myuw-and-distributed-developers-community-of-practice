# Foreshadowing the event

## Distributed Developers Community of Practice

* Email?
* Teams?

### MyUW news article on it.wisc.edu

<https://it.wisc.edu/myuw-news/>

Author a draft article there and then work with Kyle Henderson (?) to
appropriately transition to published. No expectation that this would actually
rate inclusion on it.wisc.edu news homepage or in tech news.

### UW Events Calendar (today.wisc.edu)

+ Via "Submit an event" in upper right
